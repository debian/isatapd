isatapd (0.9.7-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

  [ Bernhard Schmidt ]
  * Bump to debhelper-compat 12 (Closes: #958588)
  * Bump Standards-Version to 4.5.0, no further changes

 -- Bernhard Schmidt <berni@debian.org>  Wed, 19 Aug 2020 09:55:34 +0200

isatapd (0.9.7-4) unstable; urgency=medium

  [ Bernhard Schmidt ]
  * Depend on lsb-base for /lib/lsb/init-functions

 -- Bernhard Schmidt <berni@debian.org>  Tue, 03 Jan 2017 23:37:18 +0100

isatapd (0.9.7-3) unstable; urgency=medium

  * Fix FTBFS on Linux 4.8+ (Closes: #844869)
  * Bump Standards-Version, no changes necessary

 -- Bernhard Schmidt <berni@debian.org>  Sat, 17 Dec 2016 11:29:31 +0100

isatapd (0.9.7-2) unstable; urgency=medium

  * set default for MTU in systemd unit
  * enable all hardening flags
  * misc cleanups of debian/control
    - bump to standards 3.9.6, no changes necessary
    - migrate from github to Alioth collab-maint repository
    - migrate to berni@debian.org email address
    - drop versioned build-deps already fulfilled in oldstable
    - drop now-unversioned dpkg-dev builddep

 -- Bernhard Schmidt <berni@debian.org>  Thu, 28 Jan 2016 10:31:50 +0100

isatapd (0.9.7-1) unstable; urgency=medium

  * Imported Upstream version 0.9.7
  * set architecture to linux-any to avoid building on kfreebsd and hurd
  * bump debhelper dependency to >= 9~, drop lintian override
  * bump Standards-Version to 3.9.5, no changes necessary
  * make debian/copyright DEP5 compatible
  * Convert RUN= to properly enabled/disabled services on upgrade
  * Install systemd unit file
  * Use --quiet flag of invoke-rc.d when querying for status (Closes: #709141)
  * Use source format 3.0 (quilt)

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Mon, 18 Aug 2014 16:54:15 +0200

isatapd (0.9.6-2) unstable; urgency=low

  * switch from cdbs to debhelper9 for hardening
  * depend on $remote_fs in initscript
  * if-up/if-down/ppp-up/ppp-down: Only reload isatapd if running
    (Closes: #647175)
  * add Vcs-* control fields

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Fri, 02 Dec 2011 17:57:47 +0100

isatapd (0.9.6-1) unstable; urgency=low

  * Imported Upstream version 0.9.6 (Closes: #561637)
    - mostly rewritten
    - new options --nopmtudisc, --ttl auto, --ttl inherit, --check-dns
    - removed options --user-rs, --no-user-rs
    - unlimited router names
    - always send router solicitations in userspace
    - much more robust RS/RA code to handle possible packet loss
    - parse router advertisements and adjust interval of solicitations
    - drop privileges after initializing (option --user)
    - remove stale PRL entries from kernel
  * bump Standards-Version to 3.8.3 (no changes needed)

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Sat, 19 Dec 2009 02:25:43 +0100

isatapd (0.9.5-1) unstable; urgency=low

  * new upstream version
    - new option --user-rs, rename --no-rs to --no-user-rs
    - support for kernel-space router solicitation in kernel >= 2.6.31

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Fri, 31 Jul 2009 01:10:01 +0200

isatapd (0.9.4-1) unstable; urgency=low

  * Initial release (Closes: #535020)
    - thanks to Marco d'Itri for his support

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Tue, 07 Jul 2009 02:06:26 +0200
